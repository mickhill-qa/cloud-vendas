<?php
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *
 *
 */

class erroController extends PageController
{
    public function erro404()
    {
    	$this->html->set_title('Erro 404 (Not Found) !!!');
        $this->view('erro/404');
    }




    public function erro403()
    {
    	$this->html->set_title('Error 403 (Forbidden) !!!');
        $this->view('erro/403');
    }




    public function erro500()
    {
    	$this->html->set_title('Error 500 (Internal Server) !!!');
        $this->view('erro/500');
    }
}
