<?php header('Content-type: text/html; charset=UTF-8');
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *  Arquivo de acesso (aplicação => cliente)
 *
 */

// Caminhos relativos a esse arquivo. (Editavel)
$config['APLICACAO'] = '../app/';
$config['FRAMEWORK'] = '../core/';


// Caminhos padrões. (Não Editavel)
$config['PUBLIC']    = str_replace('\\', '/', __DIR__) . '/';
$config['APLICACAO'] = str_replace('\\', '/', realpath($config['PUBLIC'] . $config['APLICACAO'])) . '/';
$config['FRAMEWORK'] = str_replace('\\', '/', realpath($config['PUBLIC'] . $config['FRAMEWORK'])) . '/';


// Define Valores CONSTANTES no Sistema
define('CONF_FOLDERS_PUBLIC',       $config['PUBLIC']);
define('CONF_FOLDERS_APLICACAO',    $config['APLICACAO']);
define('CONF_FOLDERS_FRAMEWORK',    $config['FRAMEWORK']);


// Apaga array usado tmp
unset($config);


// Leno arquivo Central
require_once CONF_FOLDERS_FRAMEWORK  . 'FrontController.php';


// Iniciando página
$bootstrap = new FrontController();
$bootstrap->start();