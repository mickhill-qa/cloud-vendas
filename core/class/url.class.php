<?php
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *
 *
 */

class url
{
//    private $url_alias;
    protected $url_alias = 'cloud-vendas';




    // Busca o Endereço da Aplicação
    public function get_url_base($uri = '')
    {
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/';

        if ($this->url_alias !== null)
            $url .= $this->url_alias . '/';

        return $url . $uri;
    }




    // Busca o Endereço da Aplicação
    public function get_uri($indice_array = null)
    {
        $uri = $_SERVER['REQUEST_URI'];
        $uri_indice_array = false;

        // Remove o alias da URI
        if ($this->url_alias !== null)
        {
            $alias = $this->url_alias . '/';
            $uri   = str_replace($alias, '', $uri);
        }


        // Remove a 1ª barra
        $uri = substr($uri, 1);


        // Se passar um indice retornará a posição da uri
        if($indice_array !== null)
        {
            $uri = explode('/', $uri);
            foreach ($uri as $key => $value)
            {
                if ($key == $indice_array)
                {
                    $uri_indice_array = $value;
                }
            }
            return $uri_indice_array;
        }
        return $uri;
    }




    // Busca o Endereço da página atual
    public function get_url_atual()
    {
        return $this->get_url_base() . $this->get_uri();
    }




    // Redireciona página
    public function redirect_uri($uri)
    {
        echo '<script>location.href="' . $this->get_url_base($uri) . '";</script>';
        die;
    }
}
