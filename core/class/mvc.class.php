<?php
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *
 *
 */

abstract class mvc
{
    // Define variáveis de MVC
    private $default      = 'index';
    private $folder_models      = 'models/';
    private $folder_viewrs      = 'viewers/';
    private $folder_controllers = 'controllers/';

    private $extension_models      = '.php';
    private $extension_viewrs      = '.phtml';
    private $extension_controllers = '.php';

    private $complement_controllers  = 'Controller';
    public  $atual_view;





    public function __construct()
    {
        // Define padrões de MVC
        $this->folder_models        = CONF_FOLDERS_APLICACAO . $this->folder_models;
        $this->folder_viewrs        = CONF_FOLDERS_APLICACAO . $this->folder_viewrs;
        $this->folder_controllers   = CONF_FOLDERS_APLICACAO . $this->folder_controllers;
    }





    // Busca o arquivo visual da página digitada
    public function view($uri = null)
    {
        if($uri == null || $uri == '')
        {
            $this->controller('erro/500');
            echo 'Erro: Informe a view.';
            die;
        }

        $arquivo_viwer = $this->folder_viewrs . $uri . $this->extension_viewrs;

        if(!file_exists($arquivo_viwer))
        {
            $this->controller('erro/500');
            echo 'Erro: A view ('. $arquivo_viwer .') não existe.';
            die;
        }

        if($this->atual_view == null)
            $this->atual_view = $arquivo_viwer;

        return $arquivo_viwer;
    }





    // Busca o controlador da página digitada na url
    public function controller($uri_atual = null)
    {
        $uri_atual           = explode('/', $uri_atual);
        $controller_erro     = 'erro' . $this->complement_controllers;
        $method_erro         = 'erro';
        $method_erro        .= ($uri_atual[1] == '' || $uri_atual[1] == null ? '404' : $uri_atual[1]);
        $controller          = ($uri_atual[0] == '' || $uri_atual[0] == null ? $this->default : $uri_atual[0]) . $this->complement_controllers;unset($uri_atual[0]);
        $method              = ($uri_atual[1] == '' || $uri_atual[1] == null ? $this->default : $uri_atual[1]);unset($uri_atual[1]);
        $arquivo_controller  = $this->folder_controllers . $controller . $this->extension_controllers;

        switch (true)
        {
            // 1. Estou no erroController. Verifica se existe o erro específicado, se não: erro404
            case ($controller == $controller_erro):
                require_once($arquivo_controller);
                $pagina_atual = new $controller();
                if (method_exists($pagina_atual, $method_erro))
                    $method = $method_erro;
                else{$method = 'erro404';}
                break;



            // 2. Se o arquivo da url digitada Existe. Verifica se a subpágina existe, se não: erro404
            case file_exists($arquivo_controller):
                require_once($arquivo_controller);
                $pagina_atual = new $controller();
                if (!method_exists($pagina_atual, $method))
                {
                    unset($pagina_atual);
                    $controller         = $controller_erro;
                    $method             = 'erro404';
                    $arquivo_controller = $this->folder_controllers . $controller . $this->extension_controllers;
                    require_once($arquivo_controller);
                    $pagina_atual = new $controller();
                }
                break;



            // 3. Redirecionar para: erroController -> erro404
            default :
                $controller         = $controller_erro;
                $method             = $method_erro;
                $arquivo_controller = $this->folder_controllers . $controller . $this->extension_controllers;
                require_once($arquivo_controller);
                $pagina_atual = new $controller();
                if (!method_exists($pagina_atual, $method))
                    $method = 'erro404';
                break;
        }
        return $pagina_atual->$method();
    }
}