<?php
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *
 *
 */

class PageController extends mvc
{
    public  $url;
    public  $html;
    public  $dados_tmp;
    private $layout_render = false;

    public function __construct()
    {
        parent::__construct();
        $this->url  = new url();
        $this->html = new html();

        // Definição html padrão de todas as páginas
        $this->html->set_title(CONF_SETUP['NAME_APP']);
        $this->html->set_favicon($this->url->get_url_base('images/favicon.ico'));
        $this->html->set_copyright('Mick Hill Barroso De Jesus');
        $this->html->set_author('Mick Hill Barroso De Jesus');
        $this->html->set_contact('mickhill@hotmail.com.br');

        // Adicionando CSS
        $this->html->add_css($this->url->get_url_base('vendors/bootstrap/dist/css/bootstrap.min'));
        $this->html->add_css($this->url->get_url_base('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min'));
        $this->html->add_css($this->url->get_url_base('vendors/font-awesome/css/font-awesome.min'));
        $this->html->add_css($this->url->get_url_base('vendors/nprogress/nprogress'));
        $this->html->add_css($this->url->get_url_base('vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min'));
        $this->html->add_css($this->url->get_url_base('vendors/iCheck/skins/flat/blue'));
        $this->html->add_css_custom($this->url->get_url_base('build/css/custom'));
        $this->html->add_css_custom($this->url->get_url_base('css/this-app'));

        // Adicionando JS
        $this->html->add_js($this->url->get_url_base('vendors/jquery/dist/jquery.min'));
        $this->html->add_js($this->url->get_url_base('vendors/bootstrap/dist/js/bootstrap.min'));
        $this->html->add_js($this->url->get_url_base('vendors/nprogress/nprogress'));
        $this->html->add_js($this->url->get_url_base('vendors/bootstrap-progressbar/bootstrap-progressbar.min'));
        $this->html->add_js($this->url->get_url_base('vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'));
        $this->html->add_js_custom($this->url->get_url_base('build/js/custom'));
        $this->html->add_js_custom($this->url->get_url_base('js/this-app'));
    }






    public function layout($view)
    {
        $this->layout_render = true;
        return $this->view($view);
    }






    public function __destruct()
    {
        // Se existir uma View atual definida pelo controller: Execute-a
        if ($this->atual_view !== null)
        {
            $this->html->start();

            if($this->layout_render)
                require_once $this->view('layout/estrutura');

            else
                require_once $this->atual_view;

            $this->html->end();
        }
    }
}