<?php
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *
 *
 */

class html
{
    private $title;
    private $favicon;
    private $keywords;
    private $description;
    private $copyright;
    private $author;
    private $contact;
    private $css;
    private $css_custom;
    private $js;
    private $js_custom;
    private $started = false;


    public function set_title($title = null)
    {
        $this->title = '
        <title>'. $title .'</title>';
    }

    public function set_favicon($arquivo = null)
    {
        $this->favicon = '
        <link   type="image/x-icon"    href="' . $arquivo . '" rel="icon" />';
    }

    public function set_keywords($keywords = '')
    {
        $this->keywords = '
        <meta   name="keywords"        content="' . $keywords . '" />';
    }

    public function set_description($description = '')
    {
        $this->description = '
        <meta   name="description"     content="' . $description . '" />';
    }

    public function set_copyright($copyright = '')
    {
        $this->copyright = '
        <meta   name="copyright"       content="' . $copyright . '" />';
    }

    public function set_author($author = '')
    {
        $this->author = '
        <meta   name="author"          content="' . $author . '" />';
    }

    public function set_contact($contact = '')
    {
        $this->contact = '
        <meta   name="contact"         content="' . $contact . '" />';
    }

    public function add_css($arquivo = "")
    {
        $this->css .= '
        <link   type="text/css"        href="' . $arquivo . '.css" rel="stylesheet" />';
    }

    public function add_css_custom($arquivo = "")
    {
        $this->css_custom .= '
        <link   type="text/css"        href="' . $arquivo . '.css" rel="stylesheet" />';
    }

    public function add_js($arquivo = "")
    {
        $this->js .= '
        <script type="text/javascript" src="' . $arquivo . '.js"></script>';
    }

    public function add_js_custom($arquivo = "")
    {
        $this->js_custom .= '
        <script type="text/javascript" src="' . $arquivo . '.js"></script>';
    }





    public function start()
    {
        $this->started = true;
        echo '<!doctype html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">'
        .$this->title
        .$this->favicon
        .$this->keywords
        .$this->description
        .$this->copyright
        .$this->author
        .$this->contact
        .$this->css
        .$this->css_custom
        .'
    </head>
    <body class="nav-md">
';
    }

    public function end()
    {
        if($this->started)
            echo '
        '
        . $this->js
        . $this->js_custom
        . '
    </body>
</html>'
        ;
    }
}