<?php

const CONF_MENUS_PAINEL = array(
    3 => array(
        "nome" => "Estoque",
        "icon" => "fa-cubes",
        "uri"  => "estoque"
    ),
    1 => array(
        "nome" => "Consultar Produtos",
        "icon" => "fa-barcode",
        "uri"  => "consulta-produtos"
    ),
    2 => array(
        "nome" => "Produtos e Serviços",
        "icon" => "fa-tags",
        "uri"  => "produtos"
    ),






    array(
        "nome" => "Vendas",
        "icon" => "fa-credit-card",
        "uri"  => array(
            array(
                'nome' => 'Nova Venda',
                'icon' => 'fa-shopping-cart',
                'uri'  => 'vendas/nova-venda'
            ),
            array(
                'nome' => 'Finalizar Venda',
                'icon' => 'fa-check-square-o',
                'uri'  => 'vendas/finalizar-venda'
            ),
            array(
                'nome' => 'Cancelar Venda',
                'icon' => 'fa-thumbs-down',
                'uri'  => 'vendas/cancelar-venda'
            ),
            array(
                'nome' => 'Relarótio das Vendas',
                'icon' => 'fa-file-text-o',
                'uri'  => 'vendas/relatorio-das-vendas'
            )
        )
    ),




    array(
        "nome" => "Cliente",
        "icon" => "fa-child",
        "uri"  => "clientes"
    ),
    array(
        "nome" => "Fornecedores",
        "icon" => "fa-truck",
        "uri"  => "fornecedores"
    ),
    array(
        "nome" => "Funcionários",
        "icon" => "fa-group",
        "uri"  => "funcionarios"
    ),




    array(
        "nome" => "Finanças",
        "icon" => "fa-money",
        "uri"  => "financeiro"
    ),







    98 => array(
        "nome" => "Usuários",
        "icon" => "fa-user",
        "uri"  => "usuarios"
    ),
    99 => array(
         "nome" => "Configurações",
         "icon" => "fa-cog",
         "url"  => "/configuracoes"
     )
);