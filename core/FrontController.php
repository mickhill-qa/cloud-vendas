<?php
/*
 *-------------------------------------------------------
 *              Simple MVC - Mick Hill
 *-------------------------------------------------------
 *
 *
 *
 */

class FrontController
{
    private $page;

    // Carrega alguns padrões da aplicação
    public function __construct()
    {
        // Ler o arquivo de SETUP
        require_once CONF_FOLDERS_FRAMEWORK . 'config/setup.php';

        // Ler as Classe do sistema
        spl_autoload_register
        (
            function ($class_name)
            {
                $class_name     = $class_name . '.class.php';
                $local_arquivo  = CONF_FOLDERS_FRAMEWORK . '/class/' . $class_name;

                if(is_file($local_arquivo))
                    require_once  $local_arquivo;

                else
                    $this->page->controller('erro/500');
            }
        );
        $this->page = new PageController();
    }



    public function start()
    {
        return $this->page->controller($this->page->url->get_uri());
    }
}